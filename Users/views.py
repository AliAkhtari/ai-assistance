import os

import requests
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from TelegramImageBot import Logger
from Users.models import SignInWithGoogle, LoginTrack, UserProfile

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
log_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), "DjangoView.log")

logger_instance = Logger.Log(logger_name=__name__, log_file=log_file)
logger_instance.setup_logger()
logger = logger_instance.logger


def get_client_ip(request) -> str:
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return str(ip)


def get_user_agent(request) -> str:
    try:
        user_agent = request.META['HTTP_USER_AGENT']
    except:
        user_agent = ''
        logger.exception('''Getting User Agent Problem.''')
    return str(user_agent)


def get_ip_country(ip):
    data = requests.get(
        url='https://ipapi.co/{ip}/json/'.format(ip=ip))
    print(data.json())
    try:
        return data.json()['country_code']
    except:
        return ''


def login_view(request, *args, **kwargs):
    context = {}
    if request.method == 'POST':
        phone_number = request.POST.get('phone_number', '')
        password = request.POST.get('password', '')
        remember_me_check_box = request.POST.get('remember_me_check_box', '')
        if remember_me_check_box == 'on':
            print('YES')

    country_code = get_ip_country(ip=get_client_ip(request=request))
    context['country_code'] = country_code
    return render(request=request, template_name='LoginPage.html', context=context, content_type=None,
                  status=None,
                  using=None)


def login_with_google(request, *args, **kwargs):
    if request.is_ajax() and request.method == "POST":
        email = request.POST.get('Email', '')
        try:
            gmail_instance = SignInWithGoogle.objects.get(Email=email)
            user_name = gmail_instance.user.username
            user = User.objects.get(username=user_name)
            login_track = LoginTrack.objects.create(user=user, IP=get_client_ip(request=request),
                                                    User_agent=get_user_agent(request=request), Login_Method='Gmail')
            login_track.save()
            user_profile = UserProfile.objects.get(user=user)
            user_profile.Login_Track.add(login_track)
            user_profile.save()
            gmail_instance.Login_Track.add(login_track)
            gmail_instance.save()

            login(request, user)
            data = {"Login": "True"}

        except SignInWithGoogle.DoesNotExist:
            data = {"Login": "False"}
        except:
            logger.exception('''login_with_google Problem.''')
            data = {"Login": "False"}

        return JsonResponse(data=data, status=200)

    else:
        return JsonResponse(data={}, status=404)


def user_panel_home_page(request, *args, **kwargs):
    context = {}

    return render(request=request, template_name='LoginPage.html', context=context, content_type=None,
                  status=None,
                  using=None)
