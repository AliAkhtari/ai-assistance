from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Services(models.Model):
    Service_Name = models.CharField(max_length=64, blank=True, null=True,
                                    help_text='''Service Name''')
    Description = models.TextField(max_length=1024, blank=True, null=True,
                                   help_text='''Describe Service.''')
    Credit = models.CharField(max_length=32, blank=True, null=True,
                              help_text='''How many credit will this plan use.''')
    Language_choices = (
        ('Fa', "Fa"),
        ('En', "En"),
    )
    Language = models.CharField(max_length=8, blank=True, null=True, choices=Language_choices,
                                help_text='''Language must be on of these options:1) Fa\n\n2)En''')

    def __str__(self):
        return '{} {}'.format(self.Service_Name, self.Credit)


class SMSVerification(models.Model):
    Status_Choices = (
        ('Verified', "Verified"),
        ('Expired', "Expired"),
        ('Pending', "Pending"),
    )
    Status = models.CharField(max_length=12, choices=Status_Choices, blank=False, null=True, help_text='''Status 
    should be one of these options:1) Verified\n2) Pending\n3) Expired''')
    Token = models.TextField(max_length=32, blank=True, null=True, help_text='''Token Unique generated for each user 
        email verification.''')
    Phone_number = models.EmailField(max_length=128, blank=False, null=True,
                                     help_text='''User should enter his/her phone number.''')
    Created_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=True, null=True,
                                        help_text='''Auto now add: True.''')
    Activation_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=False, auto_now=False, null=True,
                                           help_text='''Clicked on Verification URL time.''')

    NumberOfSentSMS = models.PositiveIntegerField(default=0, blank=True, null=True, help_text='''Default: 0.''')
    user = models.OneToOneField(User, on_delete=models.SET_NULL, blank=True, null=True,
                                help_text='''On delete: Set null.Link to Django Built-in User Model''')

    def __str__(self):
        return '{} - {}'.format(self.Phone_number, self.Status)


class EmailVerification(models.Model):
    Status_Choices = (
        ('Verified', "Verified"),
        ('Expired', "Expired"),
        ('Pending', "Pending"),
    )
    Status = models.CharField(max_length=12, choices=Status_Choices, blank=True, null=True, help_text='''Status 
    should be one of these options:1) Verified\n2) Pending\n3) Expired''')
    Token = models.TextField(max_length=32, blank=True, null=True, help_text='''Token Unique generated for each user 
    email verification.''')
    Email = models.EmailField(max_length=128, blank=False, null=True, help_text='''User should enter his/her email.''')
    Created_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=True, null=True,
                                        help_text='''Auto now add: True.''')
    Activation_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=False, auto_now=False, null=True,
                                           help_text='''Clicked on Verification URL time.''')

    NumberOfSentEmails = models.PositiveIntegerField(default=0, blank=True, null=True, help_text='''Default: 0.''')
    user = models.OneToOneField(User, on_delete=models.SET_NULL, blank=True, null=True,
                                help_text='''On delete: Set null.Link to Django Built-in User Model''')

    def __str__(self):
        return '{} - {}'.format(self.Email, self.Status)


class UsageTimes(models.Model):
    Usage_times = models.DateTimeField(blank=True, editable=True, auto_now=True, null=True,
                                       help_text='''Auto now add: True.''')


class UsageHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True,
                             help_text='''On delete: Set nullLink to Django Built-in User Model''')
    Service = models.OneToOneField(Services, on_delete=models.SET_NULL, blank=True, null=True,
                                   help_text='''On delete: Set null.Linked to Services to define.Which service user 
                                   used.''')
    Usage_times = models.ManyToManyField(UsageTimes, blank=True, help_text='''Link to UsageTimes model''')
    Counter = models.PositiveIntegerField(default=0, blank=True, null=True, help_text='''Number of using service .''')

    def __str__(self):
        return '{} {}'.format(self.Service, self.Counter)


class LoginTrack(models.Model):
    LOGIN_METHODS = (
        ('Gmail', 'Gmail'),
        ('Github', 'Github'),
        ('Apple', 'Apple'),
        ('PhoneNumber', 'PhoneNumber'),
    )

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True,
                             help_text='''GenericIPAddressField''')
    IP = models.GenericIPAddressField(blank=True, null=True, help_text='''Store IP''')
    Time_stamp = models.DateTimeField(auto_now_add=True, blank=True, null=True, help_text='''Time Stamp''')
    User_agent = models.TextField(max_length=1024, null=True, blank=True, help_text='''Store User Agent.''')
    Login_Method = models.CharField(choices=LOGIN_METHODS, max_length=16, blank=False, default='PhoneNumber', null=True)

    def __str__(self):
        if self.user:
            return '{} {}'.format(self.user.username, self.IP)
        else:
            return str(self.IP)


class ShopPlans(models.Model):
    Plan_Name = models.CharField(max_length=64, default='', blank=False, null=True,
                                 help_text='''Plan Name''')
    Description = models.TextField(max_length=1024, default='', blank=False, null=True,
                                   help_text='''Describe plan.''')
    Credit = models.PositiveIntegerField(default=0, blank=False, null=False,
                                         help_text='''How many credit this plan offers.''')
    Final_Price = models.CharField(max_length=32, default='', blank=False, null=True,
                                   help_text='''Final Price of this plan.''')
    Discount_Price = models.CharField(max_length=32, default='', blank=False, null=True,
                                      help_text='''How much user will save if buy this plan.''')
    Discount_Percentage = models.CharField(max_length=4, default='', blank=False, null=True,
                                           help_text='''How much user will save if buy this plan.''')
    Language_choices = (
        ('Fa', "Fa"),
        ('En', "En"),
    )
    Language = models.CharField(max_length=8, default='', blank=False, null=True, choices=Language_choices,
                                help_text='''Language must be on of these options:1) Fa\n\n2)En''')

    def __str__(self):
        return '{} {} {}'.format(self.Plan_Name, self.Credit, self.Final_Price)


class Payments(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True,
                             help_text='''On delete: Set nullLink to Django Built-in User Model''')
    Quantity = models.PositiveIntegerField(default=0, blank=True, null=True,
                                           help_text='''How many credit user bought.''')
    Created_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=True, null=True,
                                        help_text='''Auto now add: True.''')
    Payment_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=False, auto_now=False, null=True,
                                        help_text='''Paid time stamp.''')
    Final_Price = models.CharField(max_length=32, blank=True, null=True,
                                   help_text='''Final price of Cart.''')
    Plan = models.ForeignKey(ShopPlans, on_delete=models.SET_NULL, blank=True, null=True,
                             help_text='''On delete: Set null.Linked to Shop plans.''')

    def __str__(self):
        return '{} {}'.format(self.Quantity, self.Final_Price)


class SignInWithGoogle(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    UserID = models.CharField(max_length=128, blank=False, null=True)
    Created_Date = models.DateTimeField(blank=True, editable=True, auto_now_add=True, null=True,
                                        help_text='''Auto now add: True.''')
    Name = models.CharField(max_length=128, blank=False, null=True)
    GivenName = models.CharField(max_length=128, blank=False, null=True)
    FamilyName = models.CharField(max_length=128, blank=False, null=True)
    ImageUrl = models.CharField(max_length=128, blank=False, null=True)
    Email = models.CharField(max_length=128, blank=False, null=True)
    Login_Track = models.ManyToManyField(LoginTrack, blank=True,
                                         help_text=''' Link to login track model to track user login to website.''')

    def __str__(self):
        return '{} {} {}'.format(self.user.username, self.Email, str(self.Created_Date))


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, blank=True, null=True, help_text='''On delete: Set 
    nullLink User Profile to Django Built-in User Instance''')
    Telegram_First_name = models.CharField(max_length=128, blank=True, null=True,
                                           help_text='''Get User First Name from his/her Telegram Account.Set Blank 
                                           if not available''')
    Telegram_Last_name = models.CharField(max_length=128, blank=True, null=True,
                                          help_text='''Get User Last Name from his/her Telegram Account.Set Blank if 
                                          not available ''')
    Telegram_User_name = models.CharField(max_length=128, blank=True, null=True,
                                          help_text='''Get User Username from his/her Telegram Account.Set Blank if 
                                          not available''')
    Telegram_User_id = models.CharField(max_length=128, blank=True, null=True,
                                        help_text='''Get User User-ID from his/her Telegram Account.''')
    First_name = models.CharField(max_length=128, blank=True, null=True, help_text='''User Should Enter his/her First 
    Name.Default is his/her Telegram First ''')
    Last_name = models.CharField(max_length=128, blank=True, null=True,
                                 help_text='''User Should Enter his/her Last Name.Default is his/her Telegram Last 
                                 Name, Set Blank if not available.''')
    Email = models.EmailField(max_length=128, blank=True, null=True, help_text='''User Should Enter his/her Email.''')
    Email_Verification = models.OneToOneField(EmailVerification, on_delete=models.SET_NULL, blank=True, null=True,
                                              help_text='''On delete: Set nullLink to Email Verification Model''')
    Phone_number = models.CharField(max_length=128, blank=True, null=True,
                                    help_text='''User Should Enter his/her Phone number.Default is telegram shared 
                                    phone number.''')
    Phone_number_verification = models.OneToOneField(SMSVerification, on_delete=models.SET_NULL, blank=True, null=True,
                                                     help_text='''On delete: Set nullLink to Phone Number 
                                                     Verification Model''')
    Joined_data = models.DateTimeField(auto_now_add=True, blank=True, null=True, editable=True,
                                       help_text='''Auto now add: True.''')
    Profile_Image = models.ImageField(upload_to='ProfileImages', default=r'ProfileImages\DefaultProfileImage.png')
    Is_Active = models.BooleanField(default=True, blank=True, null=True, help_text='''Default is True.''')
    Deactivation_Reason = models.TextField(max_length=1024, blank=True, null=True,
                                           help_text='''Reason why user is deactivated.''')
    Usage_History = models.ManyToManyField(UsageHistory, blank=True, help_text='''Link to Usage History model to track 
    User activity.''')
    Login_Track = models.ManyToManyField(LoginTrack, blank=True,
                                         help_text=''' Link to login track model to track user login to website.''')
    Payments = models.ManyToManyField(Payments, blank=True,
                                      help_text='''Link to Payments model to track user Payments.''')
    Credit = models.PositiveIntegerField(default=0, blank=True, null=True,
                                         help_text='''How many credit user have.''')
    Language_choices = (
        ('Fa', "Fa"),
        ('En', "En"),
    )
    Language = models.CharField(max_length=8, blank=True, null=True, choices=Language_choices,
                                help_text='''Language must be on of these options:1) Fa\n\n2)En''')
    Sex_choices = (
        ('Male', "Male"),
        ('Female', "Female"),
        ('Others', "Others"),
    )
    Sex = models.CharField(max_length=12, blank=True, null=True, choices=Sex_choices,
                           help_text='''User should choose, options are:\n1) Male\n2) Female\n3) Others''')
    Password = models.CharField(max_length=128, blank=True, null=True,
                                help_text='''Store Password for further operations.''')
    Last_menu = models.CharField(max_length=128, blank=True, null=True,
                                 help_text='''Store user last menu further operations.''')
    chosen_operation = models.CharField(max_length=128, blank=True, null=True,
                                        help_text='''Store user last menu further operations.''')
    Referral = models.ManyToManyField(User, related_name='+', blank=True)
    Gmail_Account = models.OneToOneField(SignInWithGoogle, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return '{} {} {} {}'.format(self.First_name, self.Last_name, self.Telegram_User_name, self.Credit)
