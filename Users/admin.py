from django.contrib import admin
from .models import UserProfile, Services, UsageTimes, UsageHistory, Payments, LoginTrack, SMSVerification, \
    EmailVerification, ShopPlans, SignInWithGoogle

# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Services)
admin.site.register(UsageTimes)
admin.site.register(UsageHistory)
admin.site.register(Payments)
admin.site.register(LoginTrack)
admin.site.register(SMSVerification)
admin.site.register(EmailVerification)
admin.site.register(ShopPlans)
admin.site.register(SignInWithGoogle)
