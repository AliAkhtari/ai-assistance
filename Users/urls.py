from .views import *
from django.urls import path, include

urlpatterns = [
    path('Login/', login_view, name='LoginPage'),
    path('LoginWithGoogleAjax/', login_with_google, name='LoginWithGoogleAjax'),

]
