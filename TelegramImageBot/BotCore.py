# -*- coding: utf-8 -*-

from pyrogram import *
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup
import xlsxwriter

import Logger
from DataProcessor import gender_detection, face_detection, explicit_content_detection, web_search, logo_detection, ocr, \
    text_to_voice, voice_to_text, translator
from Dialogues import *
from HandleDataBase import check_user_existence, new_user, update_user_language, get_all_user_language, \
    get_all_user_menus, update_user_menu, get_credit, get_shop_plans, add_referral_user, get_user_payment_history

logger_instance = Logger.Log(logger_name=__name__, log_file='BotCore.log')
logger_instance.setup_logger()
logger = logger_instance.logger
app = Client(
    "ai_imagebot",
    bot_token="1263096705:AAFrACh9gMjGcHy9JFEQmZo3EeW5NfOIQVQ"
)

users_data_keyboard_ids = dict()  # Store User Data, User ID as Key and [keyboard_id] as value
users_data_language = dict()  # Store User Data, User ID as Key and language as value
users_data_last_menu = dict()  # Store User Data, User ID as Key and menu as value
users_data_chosen_operation = dict()  # Store User Data, User ID as Key and chosen operation as value
shop_plans_global = []
for shop_plan in get_shop_plans():
    shop_plans_global.append(
        'پلن {}:'
        '{}'.format(shop_plan['Plan_Name'], shop_plan['Description'])
    )

for users in get_all_user_language():
    users_data_language[users[0]] = users[1]
    users_data_keyboard_ids[users[0]] = list()

for users in get_all_user_menus():
    users_data_last_menu[users[0]] = users[1]


def introduce_capabilities(user_id: str):
    if users_data_language[user_id] == 'Fa':
        app.send_message(chat_id=user_id, text=capabilities_fa)
    elif users_data_language[user_id] == 'En':
        app.send_message(chat_id=user_id, text=capabilities_en)


def referral_url(user_id: str):
    if users_data_language[user_id] == 'Fa':
        app.send_message(chat_id=user_id, text='لینک دعوت شما:')
        app.send_message(chat_id=user_id, text='دستیار هوشمند [لینک بات]({}).'.format(
            'https://t.me/ai_imagebot?start={}'.format(user_id)),
                         reply_markup=ReplyKeyboardMarkup(
                             [
                                 [return_to_previous_menu_fa],
                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ), parse_mode='markdown')


def capabilities_menu(user_id: str):
    if users_data_language[user_id] == 'Fa':
        app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب کنید.',
                         reply_markup=ReplyKeyboardMarkup(
                             [
                                 [capabilities_fa_option_one],
                                 [capabilities_fa_option_two],
                                 [capabilities_fa_option_three],
                                 [capabilities_fa_option_four],
                                 [capabilities_fa_option_five],
                                 [capabilities_fa_option_six, capabilities_fa_option_seven],
                                 [capabilities_fa_option_eight],
                                 [return_to_previous_menu_fa],
                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ))
    elif users_data_language[user_id] == 'En':
        app.send_message(chat_id=user_id, text=capabilities_en)


def choose_language(user_id: str):
    users_data_keyboard_ids[str(user_id)].append(
        int(app.send_message(chat_id=user_id,
                             text='{fa_choose_language}\n{en_choose_language}'.format(
                                 fa_choose_language=Choose_language_fa,
                                 en_choose_language=Choose_language_en),
                             reply_markup=InlineKeyboardMarkup(
                                 inline_keyboard=[
                                     [InlineKeyboardButton(text='فارسی', callback_data=b'Fa')],
                                     [InlineKeyboardButton(text='English', callback_data=b'En')],
                                 ])
                             ).message_id)
    )


def menu_one(user_id: str):
    if users_data_language[user_id] == 'Fa':
        app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب کنید.',
                         reply_markup=ReplyKeyboardMarkup(
                             [
                                 [our_capabilities_fa_option_one],
                                 [user_panel_fa_option_two, open_website_fa_option_three],
                                 [about_us_fa_option_four, support_fa_option_five],
                                 [change_language_text_option_six],
                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ))
    elif users_data_language[user_id] == 'En':
        app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب کنید.',
                         reply_markup=ReplyKeyboardMarkup(
                             [
                                 [our_capabilities_en_option_one],
                                 [user_panel_en_option_two, open_website_en_option_three],
                                 [about_us_en_option_four, support_en_option_five],
                                 [change_language_text_option_six],
                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ))


def user_panel(user_id: str):
    if users_data_language[user_id] == 'Fa':
        app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب کنید.',
                         reply_markup=ReplyKeyboardMarkup(
                             [
                                 [user_panel_option_one_fa, user_panel_option_two_fa],
                                 [user_panel_option_three_fa, user_panel_option_four_fa],
                                 [return_to_previous_menu_fa],
                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ))
    elif users_data_language[user_id] == 'En':
        app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب کنید.',
                         reply_markup=ReplyKeyboardMarkup(
                             [

                             ],
                             resize_keyboard=True,
                             one_time_keyboard=False,
                         ))


def get_account_credit(user_id: str):
    if users_data_language[user_id] == 'Fa':
        credit = get_credit(user_id=user_id)
        try:
            int(credit)
            app.send_message(chat_id=user_id, text='شما به اندازه {} درخواست اعتبار دارید.'.format(credit),
                             reply_markup=ReplyKeyboardMarkup(
                                 [
                                     [return_to_previous_menu_fa],
                                 ],
                                 resize_keyboard=True,
                                 one_time_keyboard=False,
                             ))
        except:
            app.send_message(chat_id=user_id, text=some_thing_went_wrong_fa,
                             reply_markup=ReplyKeyboardMarkup(
                                 [
                                     [return_to_previous_menu_fa],
                                 ],
                                 resize_keyboard=True,
                                 one_time_keyboard=False,
                             ))
            logger.exception('''Couldn't get account credit.''')

    elif users_data_language[user_id] == 'En':
        credit = get_credit(user_id=user_id)
        try:
            int(credit)
            app.send_message(chat_id=user_id, text='You have {} Credit.'.format(credit),
                             reply_markup=ReplyKeyboardMarkup(
                                 [

                                 ],
                                 resize_keyboard=True,
                                 one_time_keyboard=False,
                             ))
        except:
            app.send_message(chat_id=user_id, text=some_thing_went_wrong_fa,
                             reply_markup=ReplyKeyboardMarkup(
                                 [
                                     [return_to_previous_menu_fa],
                                 ],
                                 resize_keyboard=True,
                                 one_time_keyboard=False,
                             ))
            logger.exception('''Couldn't get account credit.''')


def charge_account_credit(user_id: str):
    if users_data_language[user_id] == 'Fa':
        shop_plans_instance = get_shop_plans()
        shop_plans = []
        for _ in shop_plans_instance:
            if _['Language'] == 'Fa':
                shop_plans.append(
                    'پلن {}:'
                    '{}'.format(_['Plan_Name'], _['Description'])
                )
        if len(shop_plans) != 0:
            if len(shop_plans) == 1:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 2:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 3:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 4:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 5:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 6:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 7:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 8:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 9:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [shop_plans[8]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 10:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [shop_plans[8]],
                                         [shop_plans[9]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 11:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [shop_plans[8]],
                                         [shop_plans[9]],
                                         [shop_plans[10]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 12:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [shop_plans[8]],
                                         [shop_plans[9]],
                                         [shop_plans[10]],
                                         [shop_plans[11]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
            elif len(shop_plans) == 13:
                app.send_message(chat_id=user_id, text='لطفا یکی از گزینه های زیر را انتخاب نمایید.',
                                 reply_markup=ReplyKeyboardMarkup(
                                     [
                                         [shop_plans[0]],
                                         [shop_plans[1]],
                                         [shop_plans[2]],
                                         [shop_plans[3]],
                                         [shop_plans[4]],
                                         [shop_plans[5]],
                                         [shop_plans[6]],
                                         [shop_plans[7]],
                                         [shop_plans[8]],
                                         [shop_plans[9]],
                                         [shop_plans[10]],
                                         [shop_plans[11]],
                                         [shop_plans[12]],
                                         [return_to_previous_menu_fa],
                                     ],
                                     resize_keyboard=True,
                                     one_time_keyboard=False,
                                 ))
        else:
            app.send_message(chat_id=user_id, text='در حال حاظر امکان شارژ اعتبار وجود ندارد.',
                             reply_markup=ReplyKeyboardMarkup(
                                 [
                                     [return_to_previous_menu_fa],
                                 ],
                                 resize_keyboard=True,
                                 one_time_keyboard=False,
                             ))


def referral(user_id: str, invited_user_id: str):
    try:
        int(add_referral_user(user_id=user_id, invited_user_id=invited_user_id))
        if users_data_language[user_id] == 'Fa':
            app.send_message(chat_id=user_id, text=referral_new_user_fa)
    except:
        pass


@app.on_message(filters.command('start'))
def _(client, message):
    user_id = str(message.from_user.id)
    if user_id not in users_data_keyboard_ids:
        users_data_keyboard_ids[str(user_id)] = list()
    elif len(users_data_keyboard_ids[str(user_id)]) != 0:
        app.delete_messages(chat_id=user_id,
                            message_ids=users_data_keyboard_ids[user_id])
    user_existence = check_user_existence(user_id=user_id)
    try:
        first_name = message.from_user.first_name
    except:
        first_name = ''
    try:
        last_name = message.from_user.last_name
    except:
        last_name = ''
    try:
        username = message.from_user.username
    except:
        username = ''

    if user_existence:
        if users_data_language[str(user_id)] == 'Fa':
            menu_one(user_id=str(user_id))
            update_user_menu(user_id=user_id, menu='main_menu')
            users_data_last_menu[str(user_id)] = 'main_menu'
    else:
        users_data_language[str(user_id)] = ''
        choose_language(user_id=user_id)
        try:
            profile_image = app.download_media(message=app.get_profile_photos(chat_id=user_id, limit=1)[0].file_id,
                                               file_name='DownloadedProfilePictures/')
        except:
            profile_image = ''
        gender = gender_detection(name=str(first_name) + str(last_name))
        new_user(first_name=first_name, last_name=last_name, user_id=user_id, user_name=username, sex=gender,
                 profile_image_path=str(profile_image))
        menu_one(user_id=str(user_id))
        update_user_menu(user_id=user_id, menu='main_menu')
        users_data_last_menu[str(user_id)] = 'main_menu'
        try:
            referral_id = str(int(str(message.text).replace('/start ', '')))
            if check_user_existence(user_id=referral_id):
                referral(user_id=referral_id, invited_user_id=user_id)
        except:
            pass


@app.on_message(filters.regex('^' + change_language_text_option_six + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    choose_language(user_id=user_id)


def account_history(user_id: str):
    payments = get_user_payment_history(user_id=user_id)
    if users_data_language[user_id] == 'Fa':
        if payments[0] == 'ERROR':
            app.send_message(chat_id=user_id, text=you_dont_have_any_transactions_fa)
        else:
            workbook = xlsxwriter.Workbook('transactions/{}.xlsx'.format(user_id))
            worksheet = workbook.add_worksheet('Transactions')
            worksheet.write('A1', 'تاریخ')
            worksheet.write('B1', 'اعتبار')
            worksheet.write('C1', 'مبلغ')
            for i in range(1, len(payments) + 1):
                worksheet.write(i, 0, payments[i - 1]['Payment_Date'])
                worksheet.write(i, 1, payments[i - 1]['Quantity'])
                worksheet.write(i, 2, payments[i - 1]['Final_Price'])
            workbook.close()
            app.send_document(chat_id=user_id, document='transactions/{}.xlsx'.format(user_id),
                              caption='پرداخت های شما')


@app.on_message(filters.regex('^' + user_panel_option_four_fa + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    account_history(user_id=user_id)


@app.on_message(filters.regex('^' + user_panel_fa_option_two + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    user_panel(user_id=user_id)
    update_user_menu(user_id=user_id, menu='user_panel')
    users_data_last_menu[user_id] = 'user_panel'


@app.on_message(filters.regex('^' + our_capabilities_fa_option_one + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    capabilities_menu(user_id=user_id)
    update_user_menu(user_id=user_id, menu='capabilities')
    users_data_last_menu[str(user_id)] = 'capabilities'


@app.on_message(filters.regex('^' + capabilities_fa_option_one + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    face_detection()


@app.on_message(filters.regex('^' + capabilities_fa_option_two + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    explicit_content_detection()


def shop_plans_filter(_, __, data):
    if data.text in shop_plans_global:
        return data


shop_plans_filter_ = filters.create(shop_plans_filter)


@app.on_message(shop_plans_filter_)
def _(client, message):
    user_id = str(message.from_user.id)
    print(message, str(user_id))


@app.on_message(filters.regex('^' + capabilities_fa_option_three + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    web_search()


@app.on_message(filters.regex('^' + capabilities_fa_option_four + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    logo_detection()


@app.on_message(filters.regex('^' + capabilities_fa_option_five + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    ocr()


@app.on_message(filters.regex('^' + capabilities_fa_option_six + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    text_to_voice()


@app.on_message(filters.regex('^' + capabilities_fa_option_seven + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    voice_to_text()


@app.on_message(filters.regex('^' + capabilities_fa_option_eight + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    translator()


@app.on_message(filters.regex('^' + user_panel_option_one_fa + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    get_account_credit(user_id=user_id)


@app.on_message(filters.regex('^' + user_panel_option_two_fa + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    charge_account_credit(user_id=user_id)
    update_user_menu(user_id=user_id, menu='charge_account_credit')
    users_data_last_menu[user_id] = 'charge_account_credit'


@app.on_message(filters.regex('^' + user_panel_option_three_fa + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    referral_url(user_id=user_id)
    update_user_menu(user_id=user_id, menu='referral_url')
    users_data_last_menu[user_id] = 'referral_url'


@app.on_message(filters.regex('^' + return_to_previous_menu_fa + '$'))
def _(client, message):
    user_id = str(message.from_user.id)
    if users_data_last_menu[user_id] == 'main_menu':
        if users_data_language[user_id] == 'Fa':
            app.send_message(chat_id=user_id, text=unrecognized_command_fa)
        elif users_data_language[user_id] == 'En':
            app.send_message(chat_id=user_id, text=unrecognized_command_en)
    elif users_data_last_menu[user_id] == 'capabilities':
        menu_one(user_id=user_id)
        update_user_menu(user_id=user_id, menu='main_menu')
        users_data_last_menu[user_id] = 'main_menu'
    elif users_data_last_menu[user_id] == 'user_panel':
        menu_one(user_id=user_id)
        update_user_menu(user_id=user_id, menu='main_menu')
        users_data_last_menu[user_id] = 'main_menu'
    elif users_data_last_menu[user_id] == 'get_account_credit':
        user_panel(user_id=user_id)
        update_user_menu(user_id=user_id, menu='user_panel')
        users_data_last_menu[user_id] = 'user_panel'
    elif users_data_last_menu[user_id] == 'charge_account_credit':
        user_panel(user_id=user_id)
        update_user_menu(user_id=user_id, menu='user_panel')
        users_data_last_menu[user_id] = 'user_panel'
    elif users_data_last_menu[user_id] == 'referral_url':
        user_panel(user_id=user_id)
        update_user_menu(user_id=user_id, menu='user_panel')
        users_data_last_menu[user_id] = 'user_panel'
    elif users_data_last_menu[user_id] == 'account_history':
        user_panel(user_id=user_id)
        update_user_menu(user_id=user_id, menu='user_panel')
        users_data_last_menu[user_id] = 'user_panel'


@app.on_callback_query()
def callback_query_handler(client, callback_query):
    data = callback_query['data']
    user_id = callback_query['from_user']['id']
    if len(users_data_keyboard_ids[str(user_id)]) != 0:
        app.delete_messages(chat_id=user_id,
                            message_ids=users_data_keyboard_ids[str(user_id)])
    if data == 'Fa':
        update_user_language(user_id=user_id, language='Fa')
        users_data_language[str(user_id)] = 'Fa'
        app.send_message(chat_id=user_id, text=fa_language_changed_successfully)
        try:
            first_name = callback_query.from_user.first_name
        except:
            first_name = ''
        try:
            last_name = callback_query.from_user.last_name
        except:
            last_name = ''
        if first_name is None:
            if last_name is None:
                name = 'No Name'
            else:
                name = last_name
        elif last_name is None:
            name = first_name
        else:
            name = first_name + last_name

        app.send_message(chat_id=user_id, text='سلام {} عزیز.'.format(name))
    elif data == 'En':
        update_user_language(user_id=user_id, language='En')
        users_data_language[str(user_id)] = 'En'
        app.send_message(chat_id=user_id, text=en_language_changed_successfully)
        try:
            first_name = callback_query.from_user.first_name
        except:
            first_name = ''
        try:
            last_name = callback_query.from_user.last_name
        except:
            last_name = ''
        name = first_name + last_name
        app.send_message(chat_id=user_id, text='Hi Dear {}'.format(name))


app.run()
