from __future__ import absolute_import

import logging


class Log:
    def __init__(self, logger_name: str, log_file: str):
        self.logger_name = logger_name
        self.log_file = log_file
        self.logger = ''

    def setup_logger(self):
        self.logger = logging.getLogger(self.logger_name)
        self.logger.setLevel(logging.DEBUG)

        formatter = logging.Formatter(
            '%(asctime)s:%(name)s:%(funcName)s:%(levelname)s:%(lineno)d:%(module)s:%(message)s:%(process)d:%('
            'processName)s:%(thread)d:%(threadName)s')

        file_handler = logging.FileHandler(self.log_file)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)

        self.logger.addHandler(file_handler)
