import os

from django.conf import settings

from AiAssitance import settings as app_settings
import django
import secrets
import string
import Logger
import logging

logger_instance = Logger.Log(logger_name=__name__, log_file='HandleDataBase.log')
logger_instance.setup_logger()
logger = logger_instance.logger

settings.configure(INSTALLED_APPS=app_settings.INSTALLED_APPS, DATABASES=app_settings.DATABASES)
django.setup()

from Users.models import UserProfile, EmailVerification, User, SMSVerification, LoginTrack, ShopPlans


def get_secure_random_string(length: int) -> str:
    secure_str = ''.join((secrets.choice(string.ascii_letters) for _ in range(length)))
    return secure_str


def check_user_existence(user_id: str) -> bool:
    """
    Check if user exist or not by it's Telegram User ID, return a boolean.
    Pass the user_id as string.
    return True means user exist,
    return False means user doesn't exist.
    """
    try:
        UserProfile.objects.get(Telegram_User_id=user_id)
        return True
    except UserProfile.DoesNotExist:
        return False
    except:
        logger.exception('''Couldn't check user existence.''')
        return False


def new_user(user_id: str, first_name: str, last_name: str, profile_image_path: str, sex: str, user_name: str) -> bool:
    """
    Create new user in DataBase,
    List of data should be provided:
    1) user_id as str
    2) first_name as str
    3) last_name as str
    4) profile_image_path as str
    5) sex as str
    6) user name as str
    """
    try:
        password = get_secure_random_string(8)
        user_instance = User.objects.create_user(
            username=user_id,
            password=password,
        )
        user_profile_instance = UserProfile.objects.create(
            Telegram_User_id=user_id,
            Telegram_First_name=first_name,
            Telegram_Last_name=last_name,
            First_name=first_name,
            Last_name=last_name,
            user=user_instance,
            Password=password,
            Sex=sex,
            Telegram_User_name=user_name,
        )

        if profile_image_path != '':
            image_path = r'../UploadedFiles/ProfileImages/{}.jpg'.format(
                os.path.splitext(os.path.basename(profile_image_path))[0])
            os.rename(profile_image_path, image_path)
            user_profile_instance.Profile_Image = image_path

        email_verification_instance = EmailVerification.objects.create(user=user_instance)
        user_profile_instance.Email_Verification = email_verification_instance

        phone_number_verification_instance = SMSVerification.objects.create(user=user_instance)
        user_profile_instance.Phone_number_verification = phone_number_verification_instance

        login_track_instance = LoginTrack.objects.create(user=user_instance)
        user_profile_instance.Login_Track = login_track_instance

        user_profile_instance.Credit = 10

        user_profile_instance.save()

        return True
    except:
        logger.exception('''Couldn't create new user.''')
        return False


def update_user_language(user_id: str, language: str) -> str:
    """
        Get user_id and language then update user language.
        return user language as result.
        if user doesn't exist or this method face a exception, it will return "Error" as result
        """
    try:
        user = UserProfile.objects.get(Telegram_User_id=user_id)
        user.Language = language
        user.save()
        return language
    except UserProfile.DoesNotExist:
        return 'error'
    except:
        logger.exception('''Couldn't change user language.''')
        return 'error'


def get_all_user_language() -> list:
    """
                    return all users language as list
                    """
    all_users = []
    try:
        for user in UserProfile.objects.all():
            all_users.append([user.Telegram_User_id, user.Language])
    except:
        logger.exception('''Couldn't get users language.''')
    return all_users


def get_all_user_menus() -> list:
    """
                return all users menu as list
                """
    all_users = []
    try:
        for user in UserProfile.objects.all():
            all_users.append([user.Telegram_User_id, user.Last_menu])
    except:
        logger.exception('''Couldn't get users language.''')
    return all_users


def update_user_menu(user_id: str, menu: str) -> str:
    """
            Get user_id and menu then update user menu.
            return user menu as result.
            if user doesn't exist or this method face a exception, it will return "Error" as result
            """
    try:
        user = UserProfile.objects.get(Telegram_User_id=user_id)
        user.Last_menu = menu
        user.save()
        return menu
    except UserProfile.DoesNotExist:
        return 'error'
    except:
        logger.exception('''Couldn't update user menu.''')
        return 'error'


def get_credit(user_id: str) -> str:
    """
                Get user_id and return user menu as result.
                if user doesn't exist or this method face a exception, it will return "Error" as result
                """
    try:
        credit = str(UserProfile.objects.get(Telegram_User_id=user_id).Credit)
        return credit
    except UserProfile.DoesNotExist:
        return 'error'
    except:
        logger.exception('''Couldn't update user menu.''')
        return 'error'


def get_shop_plans():
    """
    Return all ShopPlans as a list.
    """
    shop_plans_instances = ShopPlans.objects.all().order_by('-Credit')
    shop_plans = []
    for shop_plan in shop_plans_instances:
        shop_plans.append({
            'Plan_Name': shop_plan.Plan_Name,
            'Description': shop_plan.Description,
            'Credit': shop_plan.Credit,
            'Final_Price': shop_plan.Final_Price,
            'Discount_Price': shop_plan.Discount_Price,
            'Discount_Percentage': shop_plan.Discount_Percentage,
            'Language': shop_plan.Language,

        })
    return shop_plans


def add_referral_user(user_id: str, invited_user_id: str) -> str:
    """
        Get user's user_id and invited_user_id then add referral and add credit to user.
        if user doesn't exist or this method face a exception, it will return "Error" as result
        """
    try:
        invited_user = User.objects.get(username=invited_user_id)
        user = UserProfile.objects.get(Telegram_User_id=user_id)
        user.Credit += 10
        user.Referral.add(invited_user)
        user.save()
        return user_id
    except User.DoesNotExist:
        return 'error'
    except UserProfile.DoesNotExist:
        return 'error'
    except:
        logger.exception('''Couldn't update referral.''')
        return 'error'


def get_user_payment_history(user_id: str) -> list:
    """
        Get user's user_id then return all user's payment history.
        if user doesn't exist or this method face a exception, it will return ["Error"] as result
        """
    try:
        user = UserProfile.objects.get(Telegram_User_id=user_id)
        user_payments_instances = user.Payments
        payments = []
        for payment in user_payments_instances.all():
            payments.append(
                {'Quantity': str(payment.Quantity), 'Payment_Date': str(payment.Payment_Date),
                 'Final_Price': str(payment.Final_Price)})
        return payments
    except:
        logger.exception('''Couldn't get user payment history.''')
        return ['ERROR']
