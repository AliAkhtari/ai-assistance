Choose_language_fa = 'لطفا زبان مورد نظر خود را انتخاب کنید.'
Choose_language_en = 'Pleas choose your appropriate language.'

capabilities_fa = 'این بات قابلیت های شگفت انگیزی در اختیار شما میذاره. مثل :\n استخراج متن از عکس و پی دی اف, ' \
                  'تشخیص چهره, تشخیص محتوای عکس, جست و جوی عکس مورد نظر در اینترنت, تشخیص لوگو, تبدیل متن به صوت و ' \
                  'بالعکس, ترجمه متن و کلی قابلیت شگفت انگیز دیگر. '
capabilities_en = ''

fa_language_changed_successfully = 'زبان شما با موفقیت تغییر پیدا کرد.'
en_language_changed_successfully = 'Your language changed successfully.'

our_capabilities_fa_option_one = 'قابلیت های بات ما'
our_capabilities_en_option_one = 'Our bot capabilities'

user_panel_fa_option_two = 'پنل کاربری'
user_panel_en_option_two = 'User panel'

open_website_fa_option_three = 'وب سایت'
open_website_en_option_three = 'web site'

about_us_fa_option_four = "درباره ما"
about_us_en_option_four = "About us"

support_fa_option_five = "پشتیبانی"
support_en_option_five = "Support"

change_language_text_option_six = 'تغییر زبان / Change language'

capabilities_fa_main_text = ''

capabilities_fa_option_one = 'شناسایی صورت'
capabilities_fa_option_two = 'شناسایی محتوای بزرگسالان'
capabilities_fa_option_three = 'جست و جوی عکس در گوکل'
capabilities_fa_option_four = 'شناسایی لوگو'
capabilities_fa_option_five = 'استخراج متن'
capabilities_fa_option_six = 'تبدیل متن به صوت'
capabilities_fa_option_seven = 'تبدیل صوت به متن'
capabilities_fa_option_eight = 'ترجمه متن'

return_to_previous_menu_fa = 'بازگشت به منوی قبلی'

unrecognized_command_fa = 'دستور ارسالی صحیح نمی باشد.'
unrecognized_command_en = 'Your Command is not true'

user_panel_option_one_fa = 'موجودی حساب'
user_panel_option_two_fa = 'شارژ حساب'
user_panel_option_three_fa = 'دریافت لینک دعوت'
user_panel_option_four_fa = 'پرداخت ها'

backup_menu_fa = ''

some_thing_went_wrong_fa = 'مشکلی پیش آمده است, لطفا با پشتیبانی تماس بکیرید.'
referral_new_user_fa = 'کابر جدیدی هم اکنون با لینک دعوت شما از بات ما استفاده کرد.'
'تعداد 10 درخواست به اعتبار شما اضافه گردید.'

you_dont_have_any_transactions_fa = 'شما هیچ تراکنشی تا الان انجام نداده اید.'
