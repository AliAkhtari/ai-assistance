from django.shortcuts import render


# Create your views here.
def home_page(request, *args, **kwargs):
    context = {}
    return render(request=request, template_name='HomePage.html', context=context, content_type=None,
                  status=None,
                  using=None)


